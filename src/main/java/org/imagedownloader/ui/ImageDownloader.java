package org.imagedownloader.ui;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.imagedownloader.controller.ImageDownloaderController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ImageDownloader {

    private static final String NO_ARGS_MSG = "No argument entered please enter an URL followed by the directory you are willing to save the images!";
    private static final String WRONG_URL = "Invalid URL, please enter a valid URL with http:// or Https:// protocol!";
    private static final String NO_DIRECTORY = "No Directory entered, please enter a valid directory path!";

    @Autowired
    private ImageDownloaderController imageDownloaderController;

    private String url;
    private String directory;

    public static void main(String[] args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "applicationContext.xml");

        ImageDownloader imageDownloader = ctx.getBean(ImageDownloader.class);
        System.console().writer().println("\n\n\n");
        imageDownloader.treatArg(args);

        try {
            if (imageDownloader.url != null && !imageDownloader.url.isEmpty()
                    && imageDownloader.directory != null
                    && !imageDownloader.directory.isEmpty()) {

                System.console().writer().println("Downloading images......................");
                System.console().writer().println(imageDownloader.url + " =====> "
                        + imageDownloader.directory);

                imageDownloader.imageDownloaderController.downloadImages(
                        imageDownloader.url, imageDownloader.directory);

                System.console().writer().println("..................Complete!");
            }
        } catch (Exception e) {
            System.console().writer().println("Something went wrong, please verify the error on the following message");
            e.printStackTrace();
        }
    }

    private void treatArg(String[] args) {

        if (args.length > 0) {
            setURL(args[0]);

            if (args.length > 1) {
                setDirectory(args[1]);
            } else {
                System.console().writer().println(NO_DIRECTORY);
            }
        } else {
            System.console().writer().println(NO_ARGS_MSG);
        }
    }

    private void setDirectory(String directory) {
        this.directory = directory;
    }

    private void setURL(String url) {
        Pattern pattern = Pattern.compile("^(http|https)://");
        Matcher matcher = pattern.matcher(url);

        if (matcher.find()) {
            this.url = url;
        } else {
            System.console().writer().println(WRONG_URL);
        }
    }

}

package org.imagedownloader.controller;

import java.io.File;

import org.imagedownloader.service.DownloaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ImageDownloaderController {

    @Autowired
    private DownloaderService downloaderService;

    public void downloadImages(String pageUrl, String outputDirectoryPath)
            throws Exception {

        File outputDirectory = new File(outputDirectoryPath);

        downloaderService.download(pageUrl, outputDirectory);

    }

}

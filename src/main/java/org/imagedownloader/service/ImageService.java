package org.imagedownloader.service;

import java.io.File;
import java.io.IOException;

public interface ImageService {

    void downloadImage(File outputDirectory) throws IOException;

}

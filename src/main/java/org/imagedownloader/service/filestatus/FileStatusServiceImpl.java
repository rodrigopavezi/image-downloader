package org.imagedownloader.service.filestatus;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.imagedownloader.io.URLWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileStatusServiceImpl implements FileStatusService {

    @Autowired
    private URLWrapper urlWrapper;

    @Override
    public boolean isFileEntitledToBeDownload(File outputDirectory)
            throws IOException {
        boolean isEntitled = false;

        String fileName = urlWrapper.getFileName();

        Path path = FileSystems.getDefault().getPath(outputDirectory.getPath(),
                fileName);
        if (Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
            Date lastModifiedDate = new Date(Files.getLastModifiedTime(path,
                    LinkOption.NOFOLLOW_LINKS).toMillis());

            if (hasChangedHttpCode(lastModifiedDate)) {
                isEntitled = true;
            }
        } else {
            isEntitled = true;
        }

        return isEntitled;
    }

    private boolean hasChangedHttpCode(Date fileLastModifiedDate) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection connection = (HttpURLConnection) urlWrapper
                    .openConnection();

            SimpleDateFormat sdf = new SimpleDateFormat(
                    "EEE, d MMM yyyy HH:mm:ss 'GMT'");
            String ifModifiedSinceString = sdf.format(fileLastModifiedDate);
            connection.setRequestMethod("HEAD");
            connection.setRequestProperty("If-Modified-Since",
                    ifModifiedSinceString);
            return (connection.getResponseCode() != HttpURLConnection.HTTP_NOT_MODIFIED);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

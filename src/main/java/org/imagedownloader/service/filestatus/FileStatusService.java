package org.imagedownloader.service.filestatus;

import java.io.File;
import java.io.IOException;

public interface FileStatusService {

    boolean isFileEntitledToBeDownload(File outputDirectory)
            throws IOException;

}

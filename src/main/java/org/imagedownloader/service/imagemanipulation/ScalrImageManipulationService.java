package org.imagedownloader.service.imagemanipulation;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.springframework.stereotype.Service;

@Service
public class ScalrImageManipulationService implements ImageManipulationService {

    @Override
    public BufferedImage resizeImage(BufferedImage originalImage,
            Dimension newImageBoudary) {

        BufferedImage scaledImg = Scalr.resize(originalImage, Method.QUALITY,
                Scalr.Mode.FIT_TO_WIDTH, newImageBoudary.width,
                newImageBoudary.height, Scalr.OP_ANTIALIAS);

        return scaledImg;
    }

}

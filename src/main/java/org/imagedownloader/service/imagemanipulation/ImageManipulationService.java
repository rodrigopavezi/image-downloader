package org.imagedownloader.service.imagemanipulation;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

public interface ImageManipulationService {

    BufferedImage resizeImage(BufferedImage originalImage,
            Dimension newImageBoudary);
}

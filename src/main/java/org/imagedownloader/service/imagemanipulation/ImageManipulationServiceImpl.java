package org.imagedownloader.service.imagemanipulation;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import org.springframework.stereotype.Service;

@Service
public class ImageManipulationServiceImpl implements ImageManipulationService {

    public BufferedImage resizeImage(BufferedImage originalImage,
            Dimension newImageBoudary) {

        int type = BufferedImage.TYPE_INT_ARGB;

        int resizedImageWidth = (newImageBoudary.width >= 0) ? newImageBoudary.width
                : originalImage.getWidth();
        int resizedImageHeight = (newImageBoudary.height >= 0) ? newImageBoudary.height
                : originalImage.getHeight();

        BufferedImage resizedImage = new BufferedImage(resizedImageWidth,
                resizedImageHeight, type);
        Graphics2D g = resizedImage.createGraphics();

        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(originalImage.getScaledInstance(newImageBoudary.width,
                newImageBoudary.height, Image.SCALE_SMOOTH), 0, 0, null);

        g.dispose();

        return resizedImage;
    }
}

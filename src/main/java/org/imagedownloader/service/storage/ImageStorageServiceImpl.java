package org.imagedownloader.service.storage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.imagedownloader.io.ImageIOWrapper;
import org.imagedownloader.model.Options;
import org.imagedownloader.service.imagemanipulation.ImageManipulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageStorageServiceImpl implements ImageStorageService {

    @Autowired
    private ImageManipulationService imageManipulationService;

    @Autowired
    private Options options;

    @Autowired
    private ImageIOWrapper imageIOWrapper;

    /**
     * Save an image on the given directory in its original state and in
     * different sizes and formats. It calls the imageManipulator service to
     * resize the images on the given sizes
     * 
     * @param inputStream
     * @param outputDirectory
     * @param fileName
     * 
     */
    public void save(InputStream inputStream, File outputDirectory,
            String fileName) throws IOException {

        BufferedImage originalImage = imageIOWrapper.read(inputStream);

        if (!isImageSmallerThanTheLimit(originalImage)) {

            String[] nameAndFormat = fileName.split("\\.");
            String name = fileName;
            String originalformat = "png";
            if (nameAndFormat.length > 1) {
                name = nameAndFormat[0];
                originalformat = nameAndFormat[1];
            }

            outputDirectory.mkdirs();

            imageIOWrapper.write(originalImage, originalformat, new File(
                    outputDirectory, fileName));

            for (String sizeString : options.getImageSizes()) {
                int size = Integer.parseInt(sizeString);

                Dimension newImageBoudary = new Dimension(size,
                        originalImage.getHeight());
                BufferedImage resizedImage = imageManipulationService
                        .resizeImage(originalImage, newImageBoudary);

                for (String format : options.getImageFormats()) {

                    switch (format) {
                    case "jpg":
                        resizedImage = getFixedForJPG(resizedImage);
                        break;
                    case "png":
                    default:
                        break;
                    }

                    String file = name + "_" + size + "." + format;
                    imageIOWrapper.write(resizedImage, format, new File(
                            outputDirectory, file));
                }
            }
        }
    }

    /**
     * Checks if image has a size greater than the one specified on the
     * smallestSizeLimit
     * 
     * @param originalImage
     * @return false or true
     */
    private boolean isImageSmallerThanTheLimit(BufferedImage originalImage) {

        boolean isSmaller = false;
        int smallestSizeLimit = options.getSmallestSizeLimit();

        if (originalImage.getWidth() < smallestSizeLimit
                || originalImage.getHeight() < smallestSizeLimit) {
            isSmaller = true;
        }

        return isSmaller;
    }

    /**
     * Add white background for JPG
     * 
     * @param image
     * @return a BufferedImage with white background
     */
    private BufferedImage getFixedForJPG(BufferedImage image) {

        BufferedImage fixedJPGImage = new BufferedImage(image.getWidth(),
                image.getHeight(), BufferedImage.TYPE_INT_RGB);
        fixedJPGImage.createGraphics()
                .drawImage(image, 0, 0, Color.WHITE, null);

        return fixedJPGImage;
    }
}

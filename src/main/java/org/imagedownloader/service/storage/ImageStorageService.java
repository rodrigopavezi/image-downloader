package org.imagedownloader.service.storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface ImageStorageService {

    void save(InputStream inputStream, File outputDirectory,
            String fileName) throws IOException;
}

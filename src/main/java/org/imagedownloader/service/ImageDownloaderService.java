package org.imagedownloader.service;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.imagedownloader.io.URLWrapper;
import org.imagedownloader.service.document.DocumentLoaderService;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageDownloaderService implements DownloaderService {

    @Autowired
    private URLWrapper urlWrapper;

    @Autowired
    private ImageService imageService;
    @Autowired
    private DocumentLoaderService documentLoaderService;

    public void download(String url, File outputDirectory) throws IOException {

        Document document = documentLoaderService.load(url);

        Elements imageSRCs = document.select("img[src]");

        // Excludes duplicate elements
        Set<String> srcSet = new HashSet<String>();

        for (Element imageSRC : imageSRCs) {
            String srcAbsUrl = imageSRC.absUrl("src");
            if (srcSet.add(srcAbsUrl)) {
                urlWrapper.setUrl(srcAbsUrl);
                imageService.downloadImage(outputDirectory);
            }
        }

    }
}

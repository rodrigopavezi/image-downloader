package org.imagedownloader.service;

import java.io.File;
import java.io.IOException;

public interface DownloaderService {

    void download(String url, File outputDirectory) throws IOException;
}

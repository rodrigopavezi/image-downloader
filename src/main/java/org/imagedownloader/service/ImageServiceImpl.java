package org.imagedownloader.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.imagedownloader.io.URLWrapper;
import org.imagedownloader.service.filestatus.FileStatusService;
import org.imagedownloader.service.storage.ImageStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private URLWrapper urlWrapper;

    @Autowired
    private FileStatusService fileStatusService;

    @Autowired
    private ImageStorageService imageStorageService;

    public void downloadImage(File outputDirectory)
            throws IOException {

        if (fileStatusService.isFileEntitledToBeDownload(outputDirectory)) {

            String fileName = urlWrapper.getFileName();
            InputStream inputStream = urlWrapper.openStream();

            imageStorageService.save(inputStream, outputDirectory, fileName);
        }
    }
}
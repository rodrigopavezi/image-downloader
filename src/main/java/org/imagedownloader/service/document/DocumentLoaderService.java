package org.imagedownloader.service.document;

import java.io.IOException;

import org.jsoup.nodes.Document;

public interface DocumentLoaderService {

    Document load(String url) throws IOException;

}

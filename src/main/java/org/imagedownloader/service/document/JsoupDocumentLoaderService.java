package org.imagedownloader.service.document;

import java.io.IOException;

import org.imagedownloader.io.JsoupWrapper;
import org.imagedownloader.model.Options;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JsoupDocumentLoaderService implements DocumentLoaderService {

    @Autowired
    private JsoupWrapper jsoupWrapper;

    @Autowired
    private Options options;

    public Document load(String url) throws IOException {

        Document docucDocument = jsoupWrapper.connect(url)
                .userAgent(options.getAgent()).get();
        return docucDocument;

    }

}

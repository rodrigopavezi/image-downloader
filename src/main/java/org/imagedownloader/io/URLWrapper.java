package org.imagedownloader.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.springframework.stereotype.Component;

@Component
public class URLWrapper {

    private URL url;

    public URLWrapper() {

    }

    public URLWrapper(String spec) throws MalformedURLException {
        url = new URL(spec);
    }

    public void setUrl(String spec) throws MalformedURLException {
        url = new URL(spec);
    }

    public InputStream openStream() throws IOException {
        return url.openStream();
    }

    public String getPath() {
        return url.getPath();
    }

    public String getFileName() {

        String src = url.getPath();
        
        int imageNameStartIndex = src.lastIndexOf('/');

        return (imageNameStartIndex > 0) ? src.substring(imageNameStartIndex,
                src.length()) : src;
    }

    public URLConnection openConnection() throws IOException {

        return url.openConnection();
    }

}

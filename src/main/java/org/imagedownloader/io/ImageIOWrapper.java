package org.imagedownloader.io;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Component;

@Component
public class ImageIOWrapper {

    public BufferedImage read(InputStream inputStream) throws IOException {
        return ImageIO.read(inputStream);
    }

    public boolean write(RenderedImage im, String formatName, File output)
            throws IOException {

        return ImageIO.write(im, formatName, output);
    }

}

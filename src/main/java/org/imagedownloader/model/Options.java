package org.imagedownloader.model;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Options {

    @Value("#{appProperties['app.storege.image.width.sizes'].split(',')}")
    private List<String> imageSizes;

    @Value("#{appProperties['app.storege.image.formats'].split(',')}")
    private List<String> imageFormats;

    @Value("#{appProperties['app.storege.image.smallest.size.limit']}")
    private int smallestSizeLimit;

    @Value("#{appProperties['jsoup.connection.agent']}")
    private String agent;

    public List<String> getImageSizes() {
        return imageSizes;
    }

    public List<String> getImageFormats() {
        return imageFormats;
    }

    public int getSmallestSizeLimit() {
        return smallestSizeLimit;
    }

    public String getAgent() {
        return agent;
    }
}

package org.imagedownloader.mock;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Component;

@Component
public class ImageMock extends BufferedImage {

    private final static int DEFAULT_SIZE = 200;
    private final static int IMAGE_TYPE = BufferedImage.TYPE_INT_ARGB;
    private String name;
    private String format;
    private File directory;

    public ImageMock() {
        super(DEFAULT_SIZE, DEFAULT_SIZE, IMAGE_TYPE);
    }

    public ImageMock(String name, String format, File directory) {
        super(DEFAULT_SIZE, DEFAULT_SIZE, IMAGE_TYPE);
        this.name = name;
        this.format = format;
        this.directory = directory;
        this.drawnNameOnTheImage();

    }

    public ImageMock(int width, int height, String name, String format,
            File directory) {
        super(width, height, IMAGE_TYPE);
        this.name = name;
        this.format = format;
        this.directory = directory;
        this.drawnNameOnTheImage();

    }

    public String getFileName() {
        return this.name + "." + this.getFormat();
    }

    public String getFullPath() {
        return this.directory.getPath() + "/" + this.name + "."
                + this.getFormat();
    }

    public String getName() {
        return name;
    }

    public String getFormat() {
        return format;
    }

    public File getDirectory() {
        return directory;
    }

    public void setName(String name) {
        this.name = name;
        this.drawnNameOnTheImage();
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    private void drawnNameOnTheImage() {
        Graphics2D g2 = this.createGraphics();
        g2.setColor(Color.LIGHT_GRAY);
        g2.fillRect(0, 0, DEFAULT_SIZE, DEFAULT_SIZE);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font(Font.DIALOG, Font.BOLD, 24));
        int middleOfImage = DEFAULT_SIZE / 2;
        g2.drawString(this.name, 0, middleOfImage);
        g2.dispose();
    }

    public InputStream getImageMockStream() throws IOException {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(this, this.format, byteArrayOutputStream);

        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public void save() throws IOException {
        directory.getParentFile().mkdirs();
        directory.mkdir();
        ImageIO.write(this, this.format, new File(directory, getFileName()));
    }
}

package org.imagedownloader.mock;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.springframework.stereotype.Component;

@Component
public class PageMock {

    private String url;
    private Document document;
    private File directory;
    private List<ImageMock> images;

    public PageMock() {
        this.images = new LinkedList<ImageMock>();
    }

    public PageMock(String url, File directory) {
        this.url = url;
        this.document = Document.createShell(url);
        this.directory = directory;
        this.images = new LinkedList<ImageMock>();

    }

    public void setUrl(String url) {
        this.url = url;
        this.document = Document.createShell(url);
    }

    public String getUrl() {
        return this.url;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public File getDirectory() {
        return directory;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    public List<ImageMock> getImages() {
        return images;
    }

    public void setImages(List<ImageMock> images) {
        this.images = images;
    }

    public void addImage(String imageSrc) throws IOException {

        Element img = new Element(Tag.valueOf("img"), document.baseUri());
        img.attr("src", imageSrc);
        document.body().appendChild(img);

        SRC src = new SRC(imageSrc);
        File directoryAndFolder = new File(this.directory, src.getFolder());
        ImageMock imageMock = new ImageMock();
        imageMock.setName(src.getName());
        imageMock.setFormat(src.getFormat());
        imageMock.setDirectory(directoryAndFolder);

        this.images.add(imageMock);
    }

    public void save() throws IOException {
        directory.getParentFile().mkdirs();
        directory.mkdir();

        int nameIndex = this.url.lastIndexOf("/");

        String fileName = (nameIndex > 0) ? this.url.substring(nameIndex + 1)
                : "";

        File pageFile = new File(directory, fileName);

        FileWriter fileWriter = new FileWriter(pageFile);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(document.toString());
        bufferedWriter.close();

        for (ImageMock image : images) {
            image.save();
        }
    }

}

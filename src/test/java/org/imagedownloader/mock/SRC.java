package org.imagedownloader.mock;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SRC {

    private String folder;
    private String name;
    private String format;

    private final static String REGEX = "([[^/\\s]/]*/)?([\\w-_]*)\\.(\\w*)";

    public SRC(String src) {
        this.applyRegex(src);
    }

    private void applyRegex(String src) {
        Pattern pattern = Pattern.compile(REGEX);

        Matcher matcher = pattern.matcher(src);

        if (matcher.matches()) {
            this.folder = (matcher.group(1) == null) ? "" : matcher.group(1);
            this.name = (matcher.group(2) == null) ? "" : matcher.group(2);
            this.format = (matcher.group(3) == null) ? "" : matcher.group(3);
        } else {
            this.folder = "";
            this.name = "";
            this.format = "";

        }

    }

    public String getFolder() {
        return folder;
    }

    public String getName() {
        return name;
    }

    public String getFormat() {
        return format;
    }

}

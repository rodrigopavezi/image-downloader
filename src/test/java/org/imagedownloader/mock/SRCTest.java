package org.imagedownloader.mock;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SRCTest {

    private String srcTestedNow = null;
    private String expectedFolder = null;
    private String expectedName = null;
    private String expectedFormat = null;

    public SRCTest(Object[] srcTestedNow, Object[] expectedValues) {
        this.srcTestedNow = (String) srcTestedNow[0];

        this.expectedFolder = (String) expectedValues[0];
        this.expectedName = (String) expectedValues[1];
        this.expectedFormat = (String) expectedValues[2];
    }

    @Parameters
    public static Collection data() {
        /* First element is the URI, second is the expected result */
        List srcToBeTested = Arrays.asList(new Object[][][] {
                { { "imageName.png" }, { "", "imageName", "png" } },
                { { "/imageName.png" }, { "/", "imageName", "png" } },
                { { "hey/hhh/mtt/images/image001.png" },
                        { "hey/hhh/mtt/images/", "image001", "png" } },
                { { "folder/folder/imageName.png" },
                        { "folder/folder/", "imageName", "png" } },
                { { "folder/folder/imageName.jpg" },
                        { "folder/folder/", "imageName", "jpg" } },
                { { "folder/folder/imageName.jpeg" },
                        { "folder/folder/", "imageName", "jpeg" } },
                { { "folder/folder/image_Name822-fds.png" },
                        { "folder/folder/", "image_Name822-fds", "png" } },
                { { "folder/folder/imageName.png8*" }, { "", "", "" } },
                { { "folder/folder/image Name.png" }, { "", "", "" } },
                { { "folder/ folder/imageName.png" }, { "", "", "" } },
                { { "folder//folder/imageName.png" },
                        { "folder//folder/", "imageName", "png" } }, });

        return srcToBeTested;
    }

    @Test
    public void test() {
        SRC src = new SRC(srcTestedNow);

        assertEquals("Testing for " + srcTestedNow, expectedFolder,
                src.getFolder());
        assertEquals("Testing for " + srcTestedNow, expectedName, src.getName());
        assertEquals("Testing for " + srcTestedNow, expectedFormat,
                src.getFormat());
    }

}

package org.imagedownloader.cucumber;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.jetty.server.Server;
import org.imagedownloader.controller.ImageDownloaderController;
import org.imagedownloader.mock.ImageMock;
import org.imagedownloader.mock.PageMock;
import org.junit.Assert;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "/org/imagedownloader/cucumber/cucumberContext.xml")
public class DownloadImagesSteps {

    private String pageUrl;
    private String outputDirectoryPath;
    private Path modifiedImagePath;
    private FileTime lastModifiedTimeOfTheModifiedFile;
    private FileTime yesterdayFileTime;

    @Autowired
    private ImageDownloaderController imageDownloaderController;

    @Autowired
    private Server server;

    @Autowired
    private PageMock pageMock;

    @Value("#{testProperties['mockpage.directory']}")
    private File mockPageDirectory;

    @Before
    public void beforeScenario() throws Exception {

        pageMock.setDirectory(mockPageDirectory);
        pageMock.getImages().clear();

        if (!server.isRunning()) {
            server.start();

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        server.stop();
                        // this will clean the output temporary folder with the
                        // generated files when the cucumber test finishes.
                        mockPageDirectory.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @After
    public void afterScenario() throws IOException {
        FileUtils.deleteDirectory(new File(outputDirectoryPath));
    }

    @Given("^a HTML page URL \"([^\"]*)\"$")
    public void a_HTML_page_URL(String pageUrl) throws Throwable {

        this.pageUrl = pageUrl;
        pageMock.setUrl(pageUrl);
    }

    @Given("^the page contains an image with SRC \"([^\"]*)\"$")
    public void the_page_contains_an_image_with_SRC(String imageSrc)
            throws Throwable {

        pageMock.addImage(imageSrc);
        pageMock.save();

    }

    @Given("^the page contains the following image SRCs:$")
    public void the_page_contains_the_following_images(List<String> imagesSRCs)
            throws Throwable {

        for (String imageSrc : imagesSRCs) {
            pageMock.addImage(imageSrc);
        }
        pageMock.save();
    }

    @Given("^an output directory \"([^\"]*)\" for the downloaded images$")
    public void an_output_directory_for_the_downloaded_images(
            String outputDirectoryPath) throws Throwable {

        this.outputDirectoryPath = outputDirectoryPath;
    }

    @When("^the image downloader runs$")
    public void the_image_downloader_runs() throws Throwable {
        runDownloader();
    }

    @Then("^a copy of the images from the HTML page can be found on the entered output directory$")
    public void a_copy_of_the_images_from_the_HTML_page_can_be_found_on_the_entered_output_directory()
            throws Throwable {

        for (ImageMock imageMock : pageMock.getImages()) {

            File file = new File(imageMock.getFullPath());

            boolean isImageFound = file.exists();

            Assert.assertTrue("The image file name '" + imageMock.getName()
                    + "' could not be found on the directory with path '"
                    + outputDirectoryPath + "'", isImageFound);
        }
    }

    @When("^the image downloader runs again$")
    public void the_image_downloader_runs_again() throws Throwable {
        runDownloader();
    }

    @Given("^the image \"([^\"]*)\" gets removed from the output directory$")
    public void the_image_gets_removed_from_the_output_directory(String imageSrc)
            throws Throwable {

        String fileName = getOnlyFileNameFromSrc(imageSrc);
        modifiedImagePath = FileSystems.getDefault().getPath(
                outputDirectoryPath, fileName);

        lastModifiedTimeOfTheModifiedFile = Files.getLastModifiedTime(
                modifiedImagePath, LinkOption.NOFOLLOW_LINKS);
        Files.delete(modifiedImagePath);
    }

    @Then("^only a copy of the removed image is downloaded$")
    public void only_a_copy_of_the_removed_image_is_downloaded()
            throws Throwable {
        assertModification();
    }

    @Given("^the directory already have the images from a previus run$")
    public void the_directory_already_have_the_images_from_a_previus_run()
            throws Throwable {

        Path path;
        yesterdayFileTime = FileTime.fromMillis(getYesterdayTime()
                .getTimeInMillis());
        // First set the last modified date on the web server
        for (ImageMock imageMock : pageMock.getImages()) {
            // Change date for images on the web page server as well
            path = FileSystems.getDefault().getPath(imageMock.getFullPath());
            Files.setLastModifiedTime(path, yesterdayFileTime);
        }
        // Run the downloader
        runDownloader();

        // Set the same last modified date as the one set previously on the web
        // server
        for (ImageMock imageMock : pageMock.getImages()) {
            // Change date for images on already download
            path = FileSystems.getDefault().getPath(outputDirectoryPath,
                    imageMock.getFileName());
            Files.setLastModifiedTime(path, yesterdayFileTime);
        }

    }

    @Given("^the image \"([^\"]*)\" gets updated on the web page$")
    public void the_image_gets_updated_on_the_web_page(String imageSrc)
            throws Throwable {
        String fileName = getOnlyFileNameFromSrc(imageSrc);

        FileTime todayFileTime = FileTime.fromMillis(getTodayTime()
                .getTimeInMillis() + 86400000);
        modifiedImagePath = FileSystems.getDefault().getPath(
                outputDirectoryPath, fileName);

        lastModifiedTimeOfTheModifiedFile = Files.getLastModifiedTime(
                modifiedImagePath, LinkOption.NOFOLLOW_LINKS);

        Path path = FileSystems.getDefault().getPath(
                mockPageDirectory.getPath(), fileName);
        Files.setLastModifiedTime(path, todayFileTime);
    }

    @Then("^only a copy of the updated image is downloaded$")
    public void only_a_copy_of_the_updated_image_is_downloaded()
            throws Throwable {
        assertModification();
    }

    private void runDownloader() throws Exception {
        imageDownloaderController.downloadImages(pageUrl, outputDirectoryPath);
    }

    private void assertModification() throws IOException {

        int comparison = 0;
        FileTime curresntLastModifiedTime;

        for (ImageMock imageMock : pageMock.getImages()) {
            Path path = FileSystems.getDefault().getPath(outputDirectoryPath,
                    imageMock.getFileName());
            curresntLastModifiedTime = Files.getLastModifiedTime(path,
                    LinkOption.NOFOLLOW_LINKS);

            if (path.equals(modifiedImagePath)) {
                comparison = curresntLastModifiedTime
                        .compareTo(lastModifiedTimeOfTheModifiedFile);
                Assert.assertTrue(comparison > 0);
            } else {
                // compare the current files last modified date with the
                // yesterday date we set previously
                comparison = curresntLastModifiedTime
                        .compareTo(yesterdayFileTime);
                Assert.assertTrue("Last modified date of the existent file "
                        + imageMock.getFileName() + " has been changed",
                        comparison < 0);
            }
        }
    }

    private String getOnlyFileNameFromSrc(String src) {
        String fileName = src;

        int imageNameStartIndex = src.lastIndexOf("/");
        if (imageNameStartIndex > 0) {
            fileName = src.substring(imageNameStartIndex, src.length());
        }
        return fileName;
    }

    private Calendar getTodayTime() {
        return Calendar.getInstance();
    }

    private Calendar getYesterdayTime() {
        Calendar timeNowToYesterday = Calendar.getInstance();
        timeNowToYesterday.add(Calendar.DATE, -1);
        return timeNowToYesterday;
    }

}

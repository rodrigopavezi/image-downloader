package org.imagedownloader.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(format = { "pretty", "html:build/cucumber-html-report" })
public class RunCukesTest {

}
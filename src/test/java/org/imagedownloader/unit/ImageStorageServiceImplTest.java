package org.imagedownloader.unit;

import static org.mockito.Mockito.*;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

import org.imagedownloader.io.ImageIOWrapper;
import org.imagedownloader.mock.ImageMock;
import org.imagedownloader.model.Options;
import org.imagedownloader.service.imagemanipulation.ImageManipulationService;
import org.imagedownloader.service.storage.ImageStorageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "/org/imagedownloader/unit/unitContext.xml")
@DirtiesContext
// *** added this @DirtiesContext annotation as a workaround for issue
// https://bitbucket.org/kubek2k/springockito/issue/37/spring-test-context-caching-confuses
public class ImageStorageServiceImplTest {

    @ReplaceWithMock
    @Autowired
    private ImageManipulationService imageManipulationService;

    @Autowired
    private ImageStorageService imageStorageService;

    @ReplaceWithMock
    @Autowired
    private Options options;

    @ReplaceWithMock
    @Autowired
    private ImageIOWrapper imageIOWrapper;

    @Test
    public void testDoesNotDownloadImagesWithSizesUnderTheLimit()
            throws IOException {

        InputStream inputStream = mock(InputStream.class);
        BufferedImage image = mock(BufferedImage.class);
        File outputDirectory = mock(File.class);
        String fileName = "mockName";

        when(imageIOWrapper.read(inputStream)).thenReturn(image);
        when(image.getWidth()).thenReturn(16);
        when(image.getHeight()).thenReturn(16);
        when(options.getSmallestSizeLimit()).thenReturn(20);

        imageStorageService.save(inputStream, outputDirectory, fileName);

        verifyZeroInteractions(imageManipulationService);

    }

    @Test
    public void testThatThreeDiferentSizesAreCreated() throws IOException {
        List<String> sizes = new LinkedList<String>();
        sizes.add("100");
        sizes.add("220");
        sizes.add("320");

        InputStream inputStream = mock(InputStream.class);
        BufferedImage image = mock(BufferedImage.class);
        File outputDirectory = spy(new File("/test/"));
        String fileName = "mockName";

        when(outputDirectory.mkdirs()).thenReturn(true);
        when(outputDirectory.getPath()).thenReturn("/dsds/ds");

        when(image.getWidth()).thenReturn(20);
        when(image.getHeight()).thenReturn(20);

        when(options.getSmallestSizeLimit()).thenReturn(16);
        when(options.getImageSizes()).thenReturn(sizes);

        when(imageIOWrapper.read(inputStream)).thenReturn(image);
        when(imageIOWrapper.write(image, "png", outputDirectory)).thenReturn(
                true);

        when(
                imageManipulationService.resizeImage(any(BufferedImage.class),
                        any(Dimension.class))).thenReturn(image);

        imageStorageService.save(inputStream, outputDirectory, fileName);

        verify(imageManipulationService, times(3)).resizeImage(
                any(BufferedImage.class), any(Dimension.class));
    }

}

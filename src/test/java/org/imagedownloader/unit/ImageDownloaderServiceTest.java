package org.imagedownloader.unit;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;

import org.imagedownloader.io.URLWrapper;
import org.imagedownloader.service.DownloaderService;
import org.imagedownloader.service.ImageService;
import org.imagedownloader.service.document.DocumentLoaderService;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.kubek2k.springockito.annotations.WrapWithSpy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "/org/imagedownloader/unit/unitContext.xml")
@DirtiesContext
// *** added this @DirtiesContext annotation as a workaround for issue
// https://bitbucket.org/kubek2k/springockito/issue/37/spring-test-context-caching-confuses
public class ImageDownloaderServiceTest {

    @Autowired
    private DownloaderService downloaderService;

    @WrapWithSpy
    @Autowired
    private URLWrapper urlWrapper;

    @ReplaceWithMock
    @Autowired
    private DocumentLoaderService documentLoaderService;

    @ReplaceWithMock
    @Autowired
    private ImageService imageService;

    @Test
    public void testDownload() throws IOException {
        String pageUrl = "http://www.imagedownloader.com/mockpage.html";
        String imageAbsoluteUrl = "http://www.imagedownloader.com/images/testimage.png";

        File outputDirectory = mock(File.class);
        Document document = mock(Document.class);
        Elements images = spy(new Elements());
        Element img = spy(new Element(Tag.valueOf("img"), ""));
        img.attr("src", "");
        images.add(img);

        when(documentLoaderService.load(pageUrl)).thenReturn(document);
        when(document.select(anyString())).thenReturn(images);
        when(img.absUrl("src")).thenReturn(imageAbsoluteUrl);

        downloaderService.download(pageUrl, outputDirectory);

        verify(documentLoaderService, atLeastOnce()).load(anyString());
        verify(urlWrapper, atLeastOnce()).setUrl(imageAbsoluteUrl);
        verify(imageService, atLeastOnce()).downloadImage(outputDirectory);

    }

    @Test
    public void testItDoesNotTryToDownloadDuplicates() throws IOException {
        String pageUrl = "http://www.imagedownloader.com/mockpage.html";
        String imageAbsoluteUrl = "http://www.imagedownloader.com/images/testimage.png";

        File outputDirectory = mock(File.class);
        Document document = mock(Document.class);
        Elements images = spy(new Elements());
        Element img = spy(new Element(Tag.valueOf("img"), ""));
        img.attr("src", "");

        Element imgDuplicate = spy(new Element(Tag.valueOf("img"), ""));
        img.attr("src", "");

        images.add(img);
        images.add(imgDuplicate);

        when(documentLoaderService.load(pageUrl)).thenReturn(document);
        when(document.select(anyString())).thenReturn(images);
        when(img.absUrl("src")).thenReturn(imageAbsoluteUrl);
        when(imgDuplicate.absUrl("src")).thenReturn(imageAbsoluteUrl);

        downloaderService.download(pageUrl, outputDirectory);

        verify(documentLoaderService).load(anyString());

        verify(urlWrapper, times(1)).setUrl(imageAbsoluteUrl);
        verify(imageService, times(1)).downloadImage(outputDirectory);

    }

}

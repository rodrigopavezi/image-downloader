package org.imagedownloader.unit;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;

import org.imagedownloader.mock.ImageMock;
import org.imagedownloader.service.imagemanipulation.ImageManipulationService;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "/org/imagedownloader/unit/unitContext.xml")
@DirtiesContext
// *** added this @DirtiesContext annotation as a workaround for issue
// https://bitbucket.org/kubek2k/springockito/issue/37/spring-test-context-caching-confuses
public class ScalrImageManipulationServiceTest {

    @Autowired
    private ImageManipulationService imageManipulationService;

    @Test
    public void testResizeImageDownwards() {

        String imageName = "mockimageForImageServiceTest";
        String fomart = "png";

        ImageMock imageMock = new ImageMock(300, 100, imageName, fomart,
                new File(""));
        Dimension newDimension = new Dimension(100, 100);

        BufferedImage resizedImage = imageManipulationService.resizeImage(
                imageMock, newDimension);

        assertEquals(resizedImage.getWidth(), newDimension.width);

        int newHeightShouldBe = 33;

        assertEquals(resizedImage.getHeight(), newHeightShouldBe);

    }

    @Test
    public void testResizeImageUpwards() {

        String imageName = "mockimageForImageServiceTest";
        String fomart = "png";

        ImageMock imageMock = new ImageMock(100, 100, imageName, fomart,
                new File(""));
        Dimension newDimension = new Dimension(300, 100);

        BufferedImage resizedImage = imageManipulationService.resizeImage(
                imageMock, newDimension);

        assertEquals(resizedImage.getWidth(), newDimension.width);

        int newHeightShouldBe = 300;

        assertEquals(resizedImage.getHeight(), newHeightShouldBe);

    }

}

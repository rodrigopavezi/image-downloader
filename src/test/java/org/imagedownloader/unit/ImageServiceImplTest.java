package org.imagedownloader.unit;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.imagedownloader.io.URLWrapper;
import org.imagedownloader.mock.ImageMock;
import org.imagedownloader.service.ImageService;
import org.imagedownloader.service.filestatus.FileStatusService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "/org/imagedownloader/unit/unitContext.xml")
@DirtiesContext
// *** added this @DirtiesContext annotation as a workaround for issue
// https://bitbucket.org/kubek2k/springockito/issue/37/spring-test-context-caching-confuses
public class ImageServiceImplTest {

    @ReplaceWithMock
    @Autowired
    private URLWrapper urlWrapper;

    @ReplaceWithMock
    @Autowired
    private FileStatusService fileStatusService;

    @Value("#{testProperties['output.directory']}")
    private File outputDirectory;

    @Autowired
    private ImageService imageService;

    private ImageMock imageMock;

    @Test
    public void testHappyPath() throws IOException {

        String imageName = "mockimageForImageServiceTest";
        String src = "/images/" + imageName + ".png";

        imageMock = new ImageMock();
        imageMock.setName(imageName);
        imageMock.setFormat("png");

        InputStream inputStream = imageMock.getImageMockStream();

        when(urlWrapper.openStream()).thenReturn(inputStream);
        when(urlWrapper.getPath()).thenReturn(src);
        when(urlWrapper.getFileName()).thenReturn(imageName + ".png");

        when(fileStatusService.isFileEntitledToBeDownload(outputDirectory))
                .thenReturn(true);

        imageService.downloadImage(outputDirectory);

        File file = new File(outputDirectory, imageMock.getFileName());

        // if files does not exists it won't be able to delete it and will throw
        // an exception
        assertTrue(file.delete());
    }

    @Test
    public void testDoNotDownloadUnmodifiedImages() throws IOException {

        urlWrapper = mock(URLWrapper.class);

        when(fileStatusService.isFileEntitledToBeDownload(outputDirectory))
                .thenReturn(false);
        // Runs the operation again
        imageService.downloadImage(outputDirectory);

        verify(urlWrapper, never()).openStream();

    }

}

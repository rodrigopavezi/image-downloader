package org.imagedownloader.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.imagedownloader.io.JsoupWrapper;
import org.imagedownloader.service.document.DocumentLoaderService;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "/org/imagedownloader/unit/unitContext.xml")
@DirtiesContext
// *** added this @DirtiesContext annotation as a workaround for issue
// https://bitbucket.org/kubek2k/springockito/issue/37/spring-test-context-caching-confuses
public class JsoupDocumentLoaderServiceTest {

    @ReplaceWithMock
    @Autowired
    private JsoupWrapper jsoupWrapper;

    @Autowired
    private DocumentLoaderService documentLoaderService;

    @Test
    /**
     * mock a document that returns from jsoup connection get
     * call service to load document
     * verify that jsoup.connect was executed
     * verify that connection.get was executed
     * assertEquals connection.get result and mocked document
     * 
     * @throws IOException
     */
    public void load() throws IOException {
        String agent = "Mozilla";
        String url = "http://testrodrigo.com:8080/testrodrigo12456.html";

        Document documentMock = mock(Document.class);
        Connection connection = mock(Connection.class);

        when(jsoupWrapper.connect(url)).thenReturn(connection);
        when(connection.userAgent("Mozilla")).thenReturn(connection);
        when(connection.get()).thenReturn(documentMock);

        Document document = documentLoaderService.load(url);

        verify(jsoupWrapper).connect(url);
        verify(connection).get();

        assertEquals(document, documentMock);

    }

}

package org.imagedownloader.unit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.imagedownloader.io.URLWrapper;
import org.imagedownloader.service.filestatus.FileStatusService;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "/org/imagedownloader/unit/unitContext.xml")
@DirtiesContext
// *** added this @DirtiesContext annotation as a workaround for issue
// https://bitbucket.org/kubek2k/springockito/issue/37/spring-test-context-caching-confuses
public class FileStatusServiceImplTest {

    @ReplaceWithMock
    @Autowired
    private URLWrapper urlWrapper;

    @Autowired
    private FileStatusService fileStatusService;

    @Value("#{testProperties['output.directory']}")
    private File outputDirectory;

    private Path path;

    @Test
    public void testFileDoesNotExistLocally() throws IOException {
        String fileName = "no.png";

        when(urlWrapper.getFileName()).thenReturn(fileName);

        boolean canFileBeDownload = fileStatusService
                .isFileEntitledToBeDownload(outputDirectory);

        assertTrue(canFileBeDownload);
    }

    @Test
    public void testFileExistsAndNotModifiedHTTPCode() throws IOException {

        // File exists locally
        this.createFile();

        // Response code has no modified code
        HttpURLConnection connection = mock(HttpURLConnection.class);
        when(urlWrapper.openConnection()).thenReturn(connection);
        when(connection.getResponseCode()).thenReturn(
                HttpURLConnection.HTTP_NOT_MODIFIED);

        boolean canFileBeDownload = fileStatusService
                .isFileEntitledToBeDownload(outputDirectory);

        verify(connection).setRequestMethod("HEAD");
        verify(connection).getResponseCode();

        assertFalse(canFileBeDownload);
    }

    @Test
    public void testFileExistsButModifiedHTTPCode() throws IOException {

        // File exists locally
        this.createFile();

        // Response code has no modified code
        HttpURLConnection connection = mock(HttpURLConnection.class);
        when(urlWrapper.openConnection()).thenReturn(connection);
        when(connection.getResponseCode())
                .thenReturn(HttpURLConnection.HTTP_OK);

        boolean canFileBeDownload = fileStatusService
                .isFileEntitledToBeDownload(outputDirectory);

        verify(connection).setRequestMethod("HEAD");
        verify(connection).getResponseCode();

        assertTrue(canFileBeDownload);
    }

    private void createFile() throws IOException {
        String fileName = "image.png";
        path = FileSystems.getDefault().getPath(outputDirectory.getPath(),
                fileName);
        Files.createDirectories(outputDirectory.toPath());
        Files.createFile(path);
        when(urlWrapper.getPath()).thenReturn(fileName);
        when(urlWrapper.getFileName()).thenReturn(fileName);
    }

    @After
    public void cleanup() throws IOException {
        if (path != null) {
            Files.delete(path);
        }
    }

}

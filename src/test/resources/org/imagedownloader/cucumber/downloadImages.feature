Feature: Download Images
	In order to have images for my mobile app 
	As a developer
	I would like to download all images defined with <img> on a HTML page
	
	Scenario: Download an image on a given HTML page hosted on unsecure website
		Given a HTML page URL "http://localhost:8080/index.html" 
		And the page contains an image with SRC "hey/hhh/hoo/images/image001.png"
		And an output directory "./build/animage" for the downloaded images
		
		When the image downloader runs
		Then a copy of the images from the HTML page can be found on the entered output directory
		
	
	Scenario: Download an image on a given HTML page URL hosted on a secure website
		Given a HTML page URL "https://localhost:8443/securepage.html"
		And the page contains an image with SRC "hey/hhh/hoo/images/image001.png"
		And an output directory "./build/securepage" for the downloaded images
		
		When the image downloader runs
		Then a copy of the images from the HTML page can be found on the entered output directory
		
		
	Scenario: Download all images on a given HTML page.
		Given a HTML page URL "http://localhost:8080/allimages.html"
		And the page contains the following image SRCs:
		|image1.png	|
		|image2.jpg	|
		|image3.gif	|
		|image4.png	|
		
		And an output directory "./build/allimages" for the downloaded images
		
		When the image downloader runs
		Then a copy of the images from the HTML page can be found on the entered output directory
	
	
	Scenario: Download only new images. 
		Given a HTML page URL "http://localhost:8080/onlyNewImages.html"
		And the page contains the following image SRCs:
		|image1.png	|
		|image2.jpg	|
		|image3.gif	|
		|image4.png	|
		
		And an output directory "./build/onlyNewImages" for the downloaded images
		And the directory already have the images from a previus run
		And the image "image2.jpg" gets removed from the output directory
		
		When the image downloader runs again
		Then only a copy of the removed image is downloaded	
	
		
	Scenario: Download only new or modified images. 
			  Images that have not been changed since tool was last run 
			  and still exist in specified directory should be omitted.
	
	Given a HTML page URL "http://localhost:8080/onlyModifiedImages.html"
		And the page contains the following image SRCs:
		|image1.png	|
		|image2.jpg	|
		|image3.gif	|
		|image4.png	|
		
		And an output directory "./build/onlyModifiedImages" for the downloaded images
		And the directory already have the images from a previus run
		And the image "image4.png" gets updated on the web page 
		
		When the image downloader runs again
		
		Then only a copy of the updated image is downloaded	
	

	
		
		
	
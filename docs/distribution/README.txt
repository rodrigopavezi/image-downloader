This is a image downloader which downloads all 
the images defined with the tag <img> on web page.

To run the application please follow the below steps:

1: cd image-downloader/bin 
2: ./image-downloader URL DIRECTORY


Example:

Linux:

1: cd image-downloader/bin
2: ./image-downloader http://ie.yahoo.com /tmp/yahoo

Windows:

1: cd image-downloader/bin
2: image-downloader.bat http://ie.yahoo.com /tmp/yahoo